package qapi

/*
 * DO NOT EDIT: This file is automatically generated
 */

// Empty implements q_empty
type Empty struct {
}

// GuestSyncDelimitedArg implements q_obj_guest-sync-delimited-arg
type GuestSyncDelimitedArg struct {
	ID int `json:"id,omitempty"`
}

// GuestSyncArg implements q_obj_guest-sync-arg
type GuestSyncArg struct {
	ID int `json:"id,omitempty"`
}

// GuestSetTimeArg implements q_obj_guest-set-time-arg
type GuestSetTimeArg struct {
	Time int `json:"time,omitempty"`
}

// GuestAgentCommandInfo implements GuestAgentCommandInfo
//
// QEMU documentation:
// Information about guest agent commands.
type GuestAgentCommandInfo struct {
	Name            string `json:"name,omitempty"`
	Enabled         bool   `json:"enabled,omitempty"`
	SuccessResponse bool   `json:"success-response,omitempty"`
}

// GuestAgentInfo implements GuestAgentInfo
//
// QEMU documentation:
// Information about guest agent.
type GuestAgentInfo struct {
	Version           string                  `json:"version,omitempty"`
	SupportedCommands []GuestAgentCommandInfo `json:"supported_commands,omitempty"`
}

// GuestShutdownArg implements q_obj_guest-shutdown-arg
type GuestShutdownArg struct {
	Mode string `json:"mode,omitempty"`
}

// GuestFileOpenArg implements q_obj_guest-file-open-arg
type GuestFileOpenArg struct {
	Path string `json:"path,omitempty"`
	Mode string `json:"mode,omitempty"`
}

// GuestFileCloseArg implements q_obj_guest-file-close-arg
type GuestFileCloseArg struct {
	Handle int `json:"handle,omitempty"`
}

// GuestFileRead implements GuestFileRead
//
// QEMU documentation:
// Result of guest agent file-read operation
type GuestFileRead struct {
	Count  int    `json:"count,omitempty"`
	BufB64 string `json:"buf-b64,omitempty"`
	EOF    bool   `json:"eof,omitempty"`
}

// GuestFileReadArg implements q_obj_guest-file-read-arg
type GuestFileReadArg struct {
	Handle int `json:"handle,omitempty"`
	Count  int `json:"count,omitempty"`
}

// GuestFileWrite implements GuestFileWrite
//
// QEMU documentation:
// Result of guest agent file-write operation
type GuestFileWrite struct {
	Count int  `json:"count,omitempty"`
	EOF   bool `json:"eof,omitempty"`
}

// GuestFileWriteArg implements q_obj_guest-file-write-arg
type GuestFileWriteArg struct {
	Handle int    `json:"handle,omitempty"`
	BufB64 string `json:"buf-b64,omitempty"`
	Count  int    `json:"count,omitempty"`
}

// GuestFileSeek implements GuestFileSeek
//
// QEMU documentation:
// Result of guest agent file-seek operation
type GuestFileSeek struct {
	Position int  `json:"position,omitempty"`
	EOF      bool `json:"eof,omitempty"`
}

// GuestFileSeekArg implements q_obj_guest-file-seek-arg
type GuestFileSeekArg struct {
	Handle int             `json:"handle,omitempty"`
	Offset int             `json:"offset,omitempty"`
	Whence GuestFileWhence `json:"whence,omitempty"`
}

// GuestFileFlushArg implements q_obj_guest-file-flush-arg
type GuestFileFlushArg struct {
	Handle int `json:"handle,omitempty"`
}

// GuestFsfreezeFreezeListArg implements q_obj_guest-fsfreeze-freeze-list-arg
type GuestFsfreezeFreezeListArg struct {
	Mountpoints []string `json:"mountpoints,omitempty"`
}

// GuestFilesystemTrimResult implements GuestFilesystemTrimResult
type GuestFilesystemTrimResult struct {
	Path    string `json:"path,omitempty"`
	Trimmed int    `json:"trimmed,omitempty"`
	Minimum int    `json:"minimum,omitempty"`
	Error   string `json:"error,omitempty"`
}

// GuestFilesystemTrimResponse implements GuestFilesystemTrimResponse
type GuestFilesystemTrimResponse struct {
	Paths []GuestFilesystemTrimResult `json:"paths,omitempty"`
}

// GuestFstrimArg implements q_obj_guest-fstrim-arg
type GuestFstrimArg struct {
	Minimum int `json:"minimum,omitempty"`
}

// GuestIPAddress implements GuestIpAddress
type GuestIPAddress struct {
	IPAddress     string             `json:"ip-address,omitempty"`
	IPAddressType GuestIPAddressType `json:"ip-address-type,omitempty"`
	Prefix        int                `json:"prefix,omitempty"`
}

// GuestNetworkInterfaceStat implements GuestNetworkInterfaceStat
type GuestNetworkInterfaceStat struct {
	RxBytes   uint64 `json:"rx-bytes,omitempty"`
	RxPackets uint64 `json:"rx-packets,omitempty"`
	RxErrs    uint64 `json:"rx-errs,omitempty"`
	RxDropped uint64 `json:"rx-dropped,omitempty"`
	TxBytes   uint64 `json:"tx-bytes,omitempty"`
	TxPackets uint64 `json:"tx-packets,omitempty"`
	TxErrs    uint64 `json:"tx-errs,omitempty"`
	TxDropped uint64 `json:"tx-dropped,omitempty"`
}

// GuestNetworkInterface implements GuestNetworkInterface
type GuestNetworkInterface struct {
	Name            string                    `json:"name,omitempty"`
	HardwareAddress string                    `json:"hardware-address,omitempty"`
	IPAddresses     []GuestIPAddress          `json:"ip-addresses,omitempty"`
	Statistics      GuestNetworkInterfaceStat `json:"statistics,omitempty"`
}

// GuestLogicalProcessor implements GuestLogicalProcessor
type GuestLogicalProcessor struct {
	LogicalID  int  `json:"logical-id,omitempty"`
	Online     bool `json:"online,omitempty"`
	CanOffline bool `json:"can-offline,omitempty"`
}

// GuestSetVcpusArg implements q_obj_guest-set-vcpus-arg
type GuestSetVcpusArg struct {
	Vcpus []GuestLogicalProcessor `json:"vcpus,omitempty"`
}

// GuestPCIAddress implements GuestPCIAddress
type GuestPCIAddress struct {
	Domain   int `json:"domain,omitempty"`
	Bus      int `json:"bus,omitempty"`
	Slot     int `json:"slot,omitempty"`
	Function int `json:"function,omitempty"`
}

// GuestDiskAddress implements GuestDiskAddress
type GuestDiskAddress struct {
	PCIController GuestPCIAddress  `json:"pci-controller,omitempty"`
	BusType       GuestDiskBusType `json:"bus-type,omitempty"`
	Bus           int              `json:"bus,omitempty"`
	Target        int              `json:"target,omitempty"`
	Unit          int              `json:"unit,omitempty"`
	Serial        string           `json:"serial,omitempty"`
	Dev           string           `json:"dev,omitempty"`
}

// GuestFilesystemInfo implements GuestFilesystemInfo
type GuestFilesystemInfo struct {
	Name       string             `json:"name,omitempty"`
	Mountpoint string             `json:"mountpoint,omitempty"`
	Type       string             `json:"type,omitempty"`
	UsedBytes  uint64             `json:"used-bytes,omitempty"`
	TotalBytes uint64             `json:"total-bytes,omitempty"`
	Disk       []GuestDiskAddress `json:"disk,omitempty"`
}

// GuestSetUserPasswordArg implements q_obj_guest-set-user-password-arg
type GuestSetUserPasswordArg struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Crypted  bool   `json:"crypted,omitempty"`
}

// GuestMemoryBlock implements GuestMemoryBlock
type GuestMemoryBlock struct {
	PhysIndex  uint64 `json:"phys-index,omitempty"`
	Online     bool   `json:"online,omitempty"`
	CanOffline bool   `json:"can-offline,omitempty"`
}

// GuestMemoryBlockResponse implements GuestMemoryBlockResponse
type GuestMemoryBlockResponse struct {
	PhysIndex uint64                       `json:"phys-index,omitempty"`
	Response  GuestMemoryBlockResponseType `json:"response,omitempty"`
	ErrorCode int                          `json:"error-code,omitempty"`
}

// GuestSetMemoryBlocksArg implements q_obj_guest-set-memory-blocks-arg
type GuestSetMemoryBlocksArg struct {
	MemBlks []GuestMemoryBlock `json:"mem-blks,omitempty"`
}

// GuestMemoryBlockInfo implements GuestMemoryBlockInfo
type GuestMemoryBlockInfo struct {
	Size uint64 `json:"size,omitempty"`
}

// GuestExecStatus implements GuestExecStatus
type GuestExecStatus struct {
	Exited       bool   `json:"exited,omitempty"`
	Exitcode     int    `json:"exitcode,omitempty"`
	Signal       int    `json:"signal,omitempty"`
	OutData      string `json:"out-data,omitempty"`
	ErrData      string `json:"err-data,omitempty"`
	OutTruncated bool   `json:"out-truncated,omitempty"`
	ErrTruncated bool   `json:"err-truncated,omitempty"`
}

// GuestExecStatusArg implements q_obj_guest-exec-status-arg
type GuestExecStatusArg struct {
	PID int `json:"pid,omitempty"`
}

// GuestExec implements GuestExec
type GuestExec struct {
	PID int `json:"pid,omitempty"`
}

// GuestExecArg implements q_obj_guest-exec-arg
type GuestExecArg struct {
	Path          string   `json:"path,omitempty"`
	Arg           []string `json:"arg,omitempty"`
	Env           []string `json:"env,omitempty"`
	InputData     string   `json:"input-data,omitempty"`
	CaptureOutput bool     `json:"capture-output,omitempty"`
}

// GuestHostName implements GuestHostName
type GuestHostName struct {
	HostName string `json:"host-name,omitempty"`
}

// GuestUser implements GuestUser
type GuestUser struct {
	User      string `json:"user,omitempty"`
	LoginTime int    `json:"login-time,omitempty"`
	Domain    string `json:"domain,omitempty"`
}

// GuestTimezone implements GuestTimezone
type GuestTimezone struct {
	Zone   string `json:"zone,omitempty"`
	Offset int    `json:"offset,omitempty"`
}

// GuestOSInfo implements GuestOSInfo
type GuestOSInfo struct {
	KernelRelease string `json:"kernel-release,omitempty"`
	KernelVersion string `json:"kernel-version,omitempty"`
	Machine       string `json:"machine,omitempty"`
	ID            string `json:"id,omitempty"`
	Name          string `json:"name,omitempty"`
	PrettyName    string `json:"pretty-name,omitempty"`
	Version       string `json:"version,omitempty"`
	VersionId     string `json:"version-id,omitempty"`
	Variant       string `json:"variant,omitempty"`
	VariantId     string `json:"variant-id,omitempty"`
}
