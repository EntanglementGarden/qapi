import logging

# Some special formatting cases
# These map between qemu built-ins and golang built-ins, as well as indicate what is a built-in
GOLANG_TYPE_MAP = {
    "uint64": "uint64",
    "str": "string",
    "int": "int",
    "bool": "bool",
    "number": "int",
}

# These are purely aesthetic
GOLANG_ENTITY_NAME_SPECIAL_CASES = {
    "Id": "ID",
    "IpAddress": "IPAddress",
    "IpAddresses": "IPAddresses",
    "IpAddressType": "IPAddressType",
    "GuestIpAddress": "GuestIPAddress",
    "GuestIpAddressType": "GuestIPAddressType",
    "GuestIpAddressList": "GuestIPAddressList",
    "GuestSuspendRam": "GuestSuspendRAM",
    "NewGuestSuspendRam": "NewGuestSuspendRAM",
    "LogicalId": "LogicalID",
    "PciController": "PCIController",
    "Eof": "EOF",
    "Ipv4": "IPv4",
    "IPv6": "IPv6",
    "GuestFsfreezeStatus": "GuestFSFreezeStatus",
    "Pid": "PID"
}

COMMAND_RETURN_TYPES = []
ENUMS = []

def loadschema(schema):
    for command in schema._entity_list:
        if not hasattr(command, 'meta'):
            continue
        if command.meta == "enum":
            ENUMS.append(command.name)
        if command.meta != "command":
            continue
        if command.ret_type is None:
            continue
        if command.ret_type.name == "int":
            continue
        if command.ret_type not  in GOLANG_TYPE_MAP:
            COMMAND_RETURN_TYPES.append(command.ret_type.name)
    logging.debug("Loaded return type list: %s", COMMAND_RETURN_TYPES)
    logging.debug("Loaded enum list: %s", ENUMS)


def getGolangType(qemuType):
    if qemuType in GOLANG_TYPE_MAP:
        return GOLANG_TYPE_MAP[qemuType]

    if qemuType.endswith("List"):
        nonListType = qemuType[:-4]
        if nonListType in GOLANG_TYPE_MAP:
            return "[]" + GOLANG_TYPE_MAP[nonListType]
        if nonListType in GOLANG_ENTITY_NAME_SPECIAL_CASES:
            return "[]" + GOLANG_ENTITY_NAME_SPECIAL_CASES[nonListType]
        return "[]" + nonListType


def golangEntityName(entity, private=True, initialized=False):
    if isinstance(entity, str):
        golangType = getGolangType(entity)
        name = entity
    else:
        golangType = getGolangType(entity.name)
        name = entity.name
    if golangType is not None:
        if initialized:
            if golangType == "int":
                return 0
            return golangType + "{}"
        return golangType
    if name.startswith("q_"):
        name = name.strip("q_obj_").strip("q_")
    if "-" in name:
        name =  name.title().replace("-", "")
    elif  "_" in name:
        name = name.title().replace("_", "")
#    if hasattr(entity, 'meta') and entity.meta == "struct" and entity.name in COMMAND_RETURN_TYPES:
#        name += "Return"
    name = (name[0].upper() if private else name[0].lower()) + name[1:]
    name = GOLANG_ENTITY_NAME_SPECIAL_CASES.get(name, name)
    logging.debug("Converted entity from %s to %s", entity if isinstance(entity, str) else entity.name, name)
    if initialized:
        if name == "int":
            return "0"
        elif entity.name in ENUMS:
            return '""'
        else:
             return name + "{}"
    return name

def formatDocString(entity, prefix=None):
    name = golangEntityName(entity)
    if prefix is not None:
        name = prefix + name
    header = "// {name} implements {entity.name}".format(name=name, entity=entity)
    if entity.doc is None or entity.doc.body.text is None:
        return header
    doc = entity.doc.body.text.strip()
    if doc == "":
        return header
    return "{}\n//\n// QEMU documentation:\n// {}".format(header, entity.doc.body.text.replace("\n", "\n// "))
