#!/usr/bin/python3
# The only parser I could find for this silly format was in python
# so this is a python script to generate Go code.
import subprocess
import logging
import os
import sys

import jinja2

import qapi.schema
import util

logging.basicConfig(level=logging.DEBUG)

env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates")))
env.filters['golangEntityName'] = util.golangEntityName
env.filters['formatDocString'] = util.formatDocString
templates = env.list_templates()

schema = qapi.schema.QAPISchema("qapi-schema.json")
util.loadschema(schema)

logging.info("Writing output file headers")


files = []
render_args = {}

for _, entity in schema._entity_dict.items():
    template_file_name = entity.meta + ".tmpl"
    if template_file_name in templates:
        if entity.meta not in render_args:
            render_args[entity.meta] = []
        render_args[entity.meta].append(entity)

for t in render_args.keys():
    filename = "{}s_gen.go".format(t)
    logging.debug("Writing %s", filename)
    with open(filename, "w") as f:
        f.write(env.get_template("{}.tmpl".format(t)).render(**render_args))
    cmd = ["gofmt", "-w", filename]
    logging.debug("executing gofmt command: %s", " ".join(cmd))
    gofmt_return = subprocess.call(cmd)
    if gofmt_return != 0:
        logging.warning("Command %s exited with code %s", cmd, gofmt_return)
        sys.exit(gofmt_return)

logging.info("completed")
