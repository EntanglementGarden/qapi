# Golang Structs for the QGA API

This package provides utilities for interacting with the QEMU guest agent.
They are generated from [the QAPI schema document](https://github.com/qemu/qemu/blob/master/qga/qapi-schema.json).

To re-generate, update `qapi-schema.json` and run `generator/generate.py`. It may
overwrite any existing files that end in `_gen.go`.
